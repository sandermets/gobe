# Learning and Informational URLS

[Go Tour](https://tour.golang.org/welcome/1)

[Go HTTP](https://golang.org/pkg/net/http/)

[Error handling](https://elithrar.github.io/article/http-handler-error-handling-revisited/)

[BasicAuth example](https://gist.github.com/elithrar/9146306)

[Restful API tutorial 1](https://thenewstack.io/make-a-restful-json-api-go/)

[Restful API tutorial 2](https://www.thepolyglotdeveloper.com/2016/07/create-a-simple-restful-api-with-golang/)

[Restful API tutorial 3](https://github.com/rfay/go_restapi_example/blob/master/restapi_example.go)

[Unit testing without a need for mocks](http://stackoverflow.com/questions/19167970/mock-functions-in-go)

[CouchDB PUT POST Issue](http://stackoverflow.com/questions/30541591/large-put-requests-from-go-to-couchdb)

[Form Data handling](https://astaxie.gitbooks.io/build-web-application-with-golang/en/04.1.html)

[CORS Tools](http://www.gorillatoolkit.org/pkg/handlers)

[CORS Examples](http://stackoverflow.com/questions/40985920/making-golang-gorilla-cors-handler-work)


## Testing with CURL

    curl -H "Origin: http://example.com/" \
        -H "Access-Control-Request-Method: POST" \
        -H "Access-Control-Request-Headers: X-Requested-With" \
        -X OPTIONS --verbose http://46.101.235.11/login
