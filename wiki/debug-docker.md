# Debug Docker

    docker build -f Dockerfile.debug -t gobe-debug:latest .

    docker run -d --name gobe-debug --privileged \
        -p 8080:8080 \
        -p 2345:2345 \
        gobe-debug:latest

    docker restart gobe-debug
