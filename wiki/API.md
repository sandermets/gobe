# API

## Postman for API calls

[Download Postman](https://www.getpostman.com/)  
[Postman collection for import](../GoBE.postman_collection.json)  

## GET /status Expect 200

    "url": "http://46.101.235.11/status",
    "method": "GET",

## POST /login Expect 200

    "url": "http://46.101.235.11/login",
    "method": "POST",
    "data": [
        {
            "key": "username",
            "value": "jane@doe.com",
            "type": "text",
            "enabled": true
        },
        {
            "key": "password",
            "value": "123456",
            "type": "text",
            "enabled": true
        }
    ],

## POST /login Expect 401

    "url": "http://46.101.235.11/login",
    "method": "POST"

## POST /login Expect 401

    "url": "http://46.101.235.11/login",
    "method": "POST",
    "data": [
        {
            "key": "username",
            "value": "dummy",
            "type": "text",
            "enabled": true
        },
        {
            "key": "password",
            "value": "dummy",
            "type": "text",
            "enabled": true
        }
    ],
