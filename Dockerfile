FROM golang:latest
LABEL Name=gobe Version=0.0.1

RUN go get github.com/gorilla/mux && \
    go get github.com/gorilla/handlers

RUN mkdir /app
ADD ./src /app/
WORKDIR /app

RUN go build -o main .
EXPOSE 8080
CMD ["/app/main"]
