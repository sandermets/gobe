package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// User : holding person structure
type User struct {
	ID       string `json:"_id,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	Domain   string `json:"domain,omitempty"`
}

// Mapping users does not allow same values
var users = make(map[string]User)

// Initial values, Todo replace with database handler
func initData() {
	users["1"] = User{ID: "1", Email: "john@world.com", Password: "123456", Domain: "hello.world.com"}
	users["2"] = User{ID: "2", Email: "jane@.world.com", Password: "123456", Domain: "hello.world.com"}
}

// GetStatus : check if system is running
func GetStatus(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(w, "I'm running!")
}

// PostLogin : Simple login returns 200 if correct username and password otherwise returns 401. Assumes x-www-form-urlencoded
func PostLogin(w http.ResponseWriter, req *http.Request) {

	var user User

	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))
	if err != nil {
		panic(err)
	}

	if err := req.Body.Close(); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &user); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	if user.Email == "" || user.Password == "" || user.Domain == "" {
		fmt.Println("Missing email or password or domain")
		http.Error(w, "Not authorized", 401)
		return
	}

	for key := range users {
		if users[key].Email == user.Email && users[key].Password == user.Password && users[key].Domain == user.Domain {
			w.WriteHeader(http.StatusOK)
			user.Password = ""
			user.ID = users[key].ID
			if err := json.NewEncoder(w).Encode(user); err != nil {
				panic(err)
			}
			return
		}
	}

	http.Error(w, "Not authorized", 401)

}

/*
With url form data
// PostLogin : Simple login returns 200 if correct username and password otherwise returns 401. Assumes x-www-form-urlencoded
func PostLogin(w http.ResponseWriter, req *http.Request) {

	req.ParseForm()

	if req.Form["username"] == nil || req.Form["password"] == nil {
		http.Error(w, "Not authorized", 401)
		return
	}

	if len(req.Form["username"]) != 1 || len(req.Form["password"]) != 1 {
		http.Error(w, "Not authorized", 401)
		return
	}

	username := req.Form["username"][0]
	password := req.Form["password"][0]

	for key := range users {
		if users[key].Username == username && users[key].Password == password {
			http.Error(w, "Logged in", 200)
			return
		}
	}
	http.Error(w, "Not authorized", 401)
}
*/

// MyServer : type definition
type MyServer struct {
	r *mux.Router
}

// CORS rules
func (s *MyServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {

	if origin := req.Header.Get("Origin"); origin != "" {
		rw.Header().Set("Access-Control-Allow-Origin", origin)
		rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		rw.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	}

	// Stop here if its Preflighted OPTIONS request
	if req.Method == "OPTIONS" {
		return
	}

	// Lets Gorilla work
	s.r.ServeHTTP(rw, req)
}

// Entrypoint
func main() {
	initData()
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/status", GetStatus).Methods("GET")
	router.HandleFunc("/login", PostLogin).Methods("POST")
	http.Handle("/", &MyServer{router})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
