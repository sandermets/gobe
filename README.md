# GoBe

## Prepare Go

[Install](https://golang.org/doc/install)  
Or use Brew  
[Set Up with Brew](http://www.golangbootcamp.com/book/get_setup)  

## Deps

    go get github.com/gorilla/mux  
    go get github.com/gorilla/handlers

## Build and Run in UNIX

    cd src/ && go build -o main . && ./main

Open [http://localhost:8080/status](http://localhost:8080/status) to verify the app is working.

## Postman for API calls

[Download Postman](https://www.getpostman.com/)  
[Postman collection for import](GoBE.postman_collection.json)  

## Loally run

With IDE Visual Studio Code and Delve installed/prepared, running and debugging is out of the box.

## Docker

[Run Docker](./wiki/run-docker.md)  
[Debug Docker](./wiki/debug-docker.md)  
[Useful URLs](./wiki/link-collection.md)  

